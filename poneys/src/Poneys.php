<?php 
  class Poneys {
      private $count = 8;



      public function getCount() {
        return $this->count;
      }

      public function setCount($number){
        $this->count = $number; 
      }

      public function removePoneyFromField($number) {
        if($this->count - $number < 0 ){
          throw new InvalidArgumentException('Nombre de Poneys ne peut etre négatif');

        }else{
          $this->count -= $number;
        }
        
      }
      
      public function addPoneyFromField($number){
      	$this->count += $number;
      }

      public function placesDisponibles($number){
        if($number >=15){
          return false;
        }else{
          return true;
        }
      }


      public function getNames() {
        
      }
  }
?>
