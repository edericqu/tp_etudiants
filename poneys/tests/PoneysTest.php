<?php
  use PHPUnit\Framework\TestCase;
  require_once 'poneys/src/Poneys.php';

  class PoneysTest extends TestCase {

    private $Poneys;

    public function setUp(){
      $this->poneys = new Poneys();
      $this->poneys->setCount(9);
    }

    public function tearDown(){
      unset($this->poney);
    }
    

    /**
    *@dataProvider providerRemovePoneyFromField
    */
    public function test_removePoneyFromField($suppression, $result) {
      ///$this->expectException(InvalidArgumentException::class);
      // Setup
      $Poneys = new Poneys();

      // Action
      $Poneys->removePoneyFromField($suppression);
      
      // Assert
      $this->assertEquals($result, $Poneys->getCount());
    }
    
    public function test_addPoneyFromField(){
    	// Setup
    	$Poneys = new Poneys();
    	//Action
    	$Poneys->addPoneyFromField(3);
    	//Assert
    	$this->assertEquals(11, $Poneys->getCount());
    	
    }

    /**
    * @expectedException InvalidArgumentException
    * @expectedExceptionMessage Nombre de Poneys ne peut etre négatif
    */

    public function test_removePoneyFromFieldException(){
       $Poneys = new Poneys();
       $Poneys->removePoneyFromField(9);
      }


    public function providerRemovePoneyFromField(){
      return array(
                    array(5,3),
                    array(8,0),
                    array(1,7),
                    array(0,8)
                  );    
    }

    /**
    *@dataProvider providerPlacesDisponibles
    */
    public function test_placesDisponibles($nbrponeys, $placesDispos){
      $Poneys = new Poneys();
      //$Poneys->placesDisponibles($nbrponeys);
      $this->assertEquals($placesDispos, $Poneys->placesDisponibles($nbrponeys) );

    }

    public function providerPlacesDisponibles(){
      return array(
                    array(20,false),
                    array(7, true),
                    array(4, true),
                    array(19, false)
                  );
    }

    public function test_getNames(){
      //Initialisation du mock
      $this->poneys = $this->getMockBuilder('Poneys')->getMock();

      //on lui dit ce que la fonction doit renvoyer
      $this->poneys->expects($this->exactly(1))->method('getNames')->willReturn(array('Joe','William', 'Jack', 'Averell'));

      //on vérifie que le résultat corrrespond bien
      $this->assertEquals(array('Joe','William', 'Jack', 'Averell'),$this->poneys->getNames());

    }







    }

 ?>
